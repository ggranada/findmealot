class ChangeAddressToStreetAddress < ActiveRecord::Migration
  def change
    rename_column :parking_lots, :address, :street_address
  end
end
