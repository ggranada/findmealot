class CreateParkingLots < ActiveRecord::Migration
  def change
    create_table :parking_lots do |t|
      t.string :address
      t.decimal :price_per_hour
      t.integer :free_lots
      t.decimal :distance

      t.timestamps null: false
    end
  end
end
