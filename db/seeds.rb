# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
more_parking_lots = [
  {:street_address => 'Raatuse 21', :price_per_hour => '0.50', :free_lots => 20, :distance => 2.5},
  {:street_address => 'Vanemuise 15', :price_per_hour => '0.80', :free_lots => 15, :distance => 1.2},
  {:street_address => 'Turu 2', :price_per_hour => '1.00', :free_lots => 30, :distance => 2.0}
]

more_parking_lots.each do |parking_lot|
  new_parking_lot = ParkingLot.new (parking_lot)
  new_parking_lot.save
end