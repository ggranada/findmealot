require 'rails_helper'

RSpec.describe ParkingLot, type: :model do
  context 'Finding nearest available parking lot' do
    it 'returns nil if input is nil' do
      list = ParkingLot.find_nearest_parking_lot('')
      expect(list).to be_empty
    end
    it 'returns nil if no parking lots are found' do
      list = ParkingLot.find_nearest_parking_lot('north pole')
      expect(list).to be_empty
    end
    it 'returns list if parking lots are available' do
      create(:parking_lot)
      list = ParkingLot.find_nearest_parking_lot('liivi 2')
      expect(list).not_to be_empty
    end
  end
end