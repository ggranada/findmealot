require 'faker'
FactoryGirl.define do
  factory :parking_lot do
    street_address {random_string}
    price_per_hour {rand(0.50..2.00)}
    free_lots {rand(1..30)}
    distance {rand(0.50..2.00)}
  end
end

def random_string
  ('a'..'z').to_a.shuffle.join
end 