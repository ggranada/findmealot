Feature: Search for nearest available parking lots

As a soulmate
So that I can have dinner with my soulmate
I want to be able to enter the restaurant address
And see the parking lots nearest to the restaurant

Given the following parking lots exist:
|street_address |price_per_hour |free_lots|distance |
|Turu 2         |1.00           |30       |2.0      |
|Raatuse 21     |0.50           |20       |2.5      |
|Vanemuise 15   |0.80           |15       |1.2      |

Scenario: Find available parking lots (some available)
  When I go to the "search" page for "parkinglots"
  And I enter the "Reference Address" "Liivi 2"
  And I press "Submit"
  Then I should be on the "results" page for "parkinglots"
  And I should see "List of free parking lots"
  And I should see "Vanemuise 15" before "Turu 2"
  And I should see "Turu 2" before "Raatuse 21"
  And I should see "Raatuse 21" before ""
  And I should see "Reference Address"
  And I should see "Liivi 2"
