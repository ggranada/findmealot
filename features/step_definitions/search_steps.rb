Given(/^the following parking lots exist:$/) do |table|
  table.hashes.each do |pl|
    new_pl = ParkingLot.new(pl)
    new_pl.save
  end
end

When(/^I go to the "([^"]*)" page for "([^"]*)"$/) do |page, model|
  visit "/#{model}/#{page}"  
end

When(/^I enter the "([^"]*)" "([^"]*)"$/) do |field, value|
  new_field = 'parking_lot_' + field.to_s.downcase
  new_field = new_field.tr(' ','_')
  fill_in(new_field, :with => value)
end

When(/^I press "([^"]*)"$/) do |button|
  click_button(button)
end

Then(/^I should be on the "([^"]*)" page for "([^"]*)"$/) do |page, model|
  path = "/" + model.to_s + "/" + page.to_s
  current_path = URI.parse(current_url).path
  current_path.should == path
end

Then(/^I should see "([^"]*)"$/) do |text|
  if page.respond_to? :should
    page.should have_content(text)
  else
    assert page.has_content?(text)
  end
end

Then(/^I should see "([^"]*)" before "([^"]*)"$/) do |e1, e2|
  #  ensure that that e1 occurs before e2.
  #  page.body is the entire content of the page as a string.
  expect /#{e1}.*#{e2}/m =~ page.body
end
