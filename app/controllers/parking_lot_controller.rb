class ParkingLotController < ApplicationController
  def search
  end

  def results
    @address = parking_lot_params['reference_address']
    @parking_lots = ParkingLot.find_nearest_parking_lot(@address)
    puts @parking_lots.to_json
  end

  def parking_lot_params
    params['parking_lot'].permit(:reference_address)
  end
end
