class ParkingLot < ActiveRecord::Base

  def self.find_nearest_parking_lot (address)
    @lots = ParkingLot.where('free_lots > ?', 0).order(:distance)    
  end
end
